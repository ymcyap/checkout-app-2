import Button from 'react-bootstrap/Button';
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/store";
import CartItems from './CartItems';

import { updateDiscountCode } from '../redux/slice/paymentMethodSlice';

import { MoneyBackGuarantee, DeliveryTruck } from '../assets/images';

const Sidebar = () => {
  const dispatch = useDispatch();
  const paymentMethod = useSelector((state: RootState) => state.paymentMethod);
  const shippingMethod = useSelector((state: RootState) => state.shippingMethod);
  const shippingFee = shippingMethod.shippingFee[shippingMethod.shippingType];
  const total = 88+shippingFee;
	
	return (
		<div className='sidebar'>
			<div className='sidebar-content'>
				<CartItems />
				<div className="sidebar-section">
					<div className="sidebar-section-row">
						<div className="discount-code">
              <div className="form-label-group">
                <input type="text" id="inputDiscountCode" placeholder="Discount Code" 
                  className={`form-control normal-input`} 
                  onChange={(e)=>dispatch(updateDiscountCode(e.target.value))}
                />
                <label htmlFor="inputDiscountCode">Discount Code</label>
              </div>
						</div>
						<div className="discount-button-box">
							<button className="discount-button">Apply</button>
						</div>
					</div>
				</div>
				<div className="sidebar-section-2">
					<div className="sidebar-section-row-2">
						<span>Subtotal</span>
						<span>$88.00</span>
					</div>
					<div className="sidebar-section-row-2">
						<span>Shipping</span>
						<span>${shippingFee}.00</span>
					</div>
				</div>
				<div className="sidebar-section-3">
					<div className="sidebar-section-row-2">
						<span className="total-title">Total</span>
						<div className="total-right">
							<span className="currency">USD</span>
							<span className="total-price">${total}.00</span>
						</div>
					</div>
				</div>
				<div className="sidebar-section-4">
					<div className="whyus-label">
						<span>Why choose us?</span>
					</div>
					<div className="whyus-row">
						<div className="whyus-badge">
							<MoneyBackGuarantee Width="85px" />
						</div>
						<div className="badge-description">
							<div className="badge-description-header">
								30-day Satisfaction Guarantee with Money Back
							</div>
							<div className="badge-description-body">
								If you're not satisfied with the products, we will issue a full refund without questions.
							</div>
						</div>
					</div>
					<div className="whyus-row">
						<div className="whyus-badge">
							<DeliveryTruck Width="85px" />
						</div>
						<div className="badge-description">
							<div className="badge-description-header">
								Over 20,000+ Successfully Shipped Orders
							</div>
							<div className="badge-description-body">
								We made as much happy customers as many orders we shipped. You simply have to join our big family.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Sidebar