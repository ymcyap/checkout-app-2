import { Minion, MoneyBackGuarantee, DeliveryTruck } from '../assets/images';

const CartItems = () => {
  return (
    <div className="sidebar-section-item">
      <div className="sidebar-section-item-row">
        <div className="item-stat">
          <div className="item-image">
            <span className="item-amount">1</span>
            <Minion size="62px" />
          </div>
          <div className="item-detail">
            <div className="item-name">Minion</div>
            <div className="item-description">Banana</div>
          </div>
        </div>
        <div className="item-price">$88.00</div>
      </div>
    </div>
  ) 
}

export default CartItems